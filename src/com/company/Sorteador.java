package com.company;

import java.util.ArrayList;
import java.util.Random;

public class Sorteador {
    Dado dado = new Dado(6);
    Random random = new Random();
    ArrayList<Integer> numerosSorteados = new ArrayList<>();
    InputOutput inputOutput = new InputOutput();

    private int numeroSorteado;


    public void sortear(){
        int numeroSorteado = random.nextInt(dado.getQuantidadeFaces())+1;
        inputOutput.imprimir(numeroSorteado);

    }

    public int getNumeroSorteado() {
        return numeroSorteado;
    }

    public void setNumeroSorteado(int numeroSorteado) {
        this.numeroSorteado = numeroSorteado;
    }
}